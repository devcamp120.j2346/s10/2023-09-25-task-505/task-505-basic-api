//B1: Import thư viện express
//import express from express
const express = require('express');

//B2: Khởi tạo app express
const app = new express();

//B3: Khai báo cổng để chạy api
const port = 8000;

//Cấu hình để sử dụng json
app.use(express.json());

//Khai báo các api
app.get('/', (req, res) => {
    let today = new Date();
    let message = `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`;
    //res.send(message);
    res.status(200).json({
        message
    })
})

//khai báo phương thức get
app.get('/get-method', (req, res) => {
    console.log('Get - Method');
    res.json({
        message : "Get - Method"
    })
})

//khai báo phương thức post
app.post('/post-method', (req, res) => {
    console.log('Post - Method');
    res.json({
        message : "Post - Method"
    })
})

//khai báo phương thức put
app.put('/put-method', (req, res) => {
    console.log('Put - Method');
    res.json({
        message : "Put - Method"
    })
})

//khai báo phương thức delete
app.delete('/delete-method', (req, res) => {
    console.log('Delete - Method');
    res.json({
        message : "Delete - Method"
    })
})

//khai bao get param
app.get('/get-data/:para1/:param2', (req, res) => {
    let param1 = req.params.para1;
    let param2 = req.params.param2;
    res.json({
        param1,
        param2
    })
})

//khai bao get query string
app.get('/query-data/:id', (req, res) => {
    let id = req.params.id;
    let query = req.query;
    let name = query.name
    res.json({
        id,
        name,
        query
    })
})

//khai báo request body
app.post('/update-data', (req, res) => {
    let body = req.body;
    res.json({
        body
    })
})
//B4: Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})
